{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE MultiWayIf #-}

module Lib
  ( someFunc
  )
where

import Control.Monad
import           Data.List
import           Euterpea
import           System.Random
import           Numeric.Natural
import Data.Monoid
import Data.Foldable 
import Lens.Micro
import Data.Maybe 

import CompST
import Elab

playPitches :: [[Pitch]] -> IO ()
playPitches = play . phrase [Dyn $ Loudness 60] . changeInstrument AcousticGrandPiano . foldl1' (:+:) . fmap
  (chord . fmap (\p -> Prim (Note hn p)))

someFunc :: IO ()
someFunc = do
  g <- newStdGen
  let start = Maj N1
  let prog = Prelude.take 16 $ generateProgression (start, chordRemoteDist start) g
  mapM_ (putStrLn . printChordNumeral . fst) $ prog
  playPitches (instantiateChord (C, 4) . fst <$> prog)

{-# LANGUAGE MultiParamTypeClasses #-}

module Comp where

import           Euterpea                       ( Music
                                                , Pitch
                                                , PitchClass
                                                )
import           System.Random
import           Data.List.NonEmpty
import           Numeric.Natural

import           Elab
import           CompST
import           Probability

data Vert f a
  = Point (Maybe PitchClass) (Maybe Natural)
    -- ^ Constrains a pitch, possibly with a specific octave
  | Chord [f]
    -- ^ Vertical Grouping of notes

data Hori f a
  = Group Rational [a]
    -- ^ A horizontal grouping of some order
  | Tension Integer f
    -- ^ Constrains to specific tension
  | Key PitchClass f
    -- ^ Constrains to specific key

data Hole

data Group a
  = Tup a a
  | Trip a a a
    deriving (Show, Eq)

{-# LANGUAGE MultiWayIf #-}

module Probability
  ( Probability(unProbability)
  , Distribution
  , mkProbability
  , overProbabilities
  , pickFromDist
  , mkDist
  )
where

import           Data.Map.Strict                ( Map(..) )
import qualified Data.Map.Strict               as M
import           Data.Map.Merge.Strict
import           System.Random
import           Data.Semigroup
import           Data.Monoid
import           Data.Group
import           Data.List.NonEmpty             ( NonEmpty(..) )
import qualified Data.List.NonEmpty            as NE
import           Data.Foldable
import           Control.Arrow
import           Data.Traversable

newtype Probability = MkProbability { unProbability :: Double } deriving (Show, Eq, Ord)

instance Semigroup Probability where
  (MkProbability x) <> (MkProbability y) = MkProbability ((x + y) / 2)

instance Monoid Probability where
  mempty = MkProbability 0

instance Group Probability where
  invert (MkProbability x) = MkProbability (1 - x)

mkProbability x = if
  | x > 1     -> MkProbability 1
  | x < 0     -> MkProbability 0
  | otherwise -> MkProbability x

newtype Distribution a = Distribution (Map a Probability) deriving (Show, Eq)

instance (Ord a, Semigroup a) => Semigroup (Distribution a) where
  (Distribution x) <> (Distribution y) = Distribution
    (merge preserveMissing preserveMissing (zipWithMatched (const (<>))) x y)

overProbabilities
  :: (Probability -> Probability) -> Distribution a -> Distribution a
overProbabilities f (Distribution xs) = Distribution (fmap f xs)

mkDist :: Map a Probability -> Distribution a
mkDist xs = Distribution
  $ fmap (MkProbability . (flip (/) max) . unProbability) xs
  where max = sum $ unProbability <$> xs

pickFromDist :: RandomGen g => Distribution a -> g -> Maybe (g, a)
pickFromDist (Distribution xs) g =
  (fmap . fmap) fst . sequence $ M.foldrWithKey' f (g, Nothing) xs
 where
  f next (MkProbability p) (gen, Nothing) = (gen, Just (next, p))
  f next (MkProbability p) (gen, Just (prev, weight)) =
    let (outcome, gen') = randomR (0, p + weight) gen
    in  if outcome >= weight
          then (gen', Just (next, p + weight))
          else (gen', Just (prev, p + weight))

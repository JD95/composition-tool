{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Elab where

import           Data.Tuple
import           Data.Foldable
import           Data.Char
import qualified Data.Map.Strict               as Map
import           Control.Applicative
import           Control.Arrow
import           Control.Monad.Identity
import           Control.Monad.Reader
import           Data.List                      ( intersect
                                                , foldl'
                                                , intercalate
                                                )
import           Data.List.NonEmpty             ( NonEmpty(..) )
import qualified Data.List.NonEmpty            as NE
import           Data.Maybe
import           Euterpea                       ( Music
                                                , Pitch
                                                , trans
                                                , pitch
                                                , PitchClass(..)
                                                , absPitch
                                                )
import           Numeric.Natural
import           System.Random

import           CompST
import           Intervals
import           Probability

newtype OrdChord = MkOrdChord { unOrd :: NonEmpty Pitch } deriving (Show, Eq, Ord)

intervalToPitch :: Pitch -> Natural -> Pitch
intervalToPitch base interval = trans (fromIntegral interval) base

pitchesToInterval :: Pitch -> Pitch -> Interval
pitchesToInterval x y = fromIntegral $ abs (absPitch x - absPitch y)

chordToIntervals x = zipWith pitchesToInterval (repeat $ NE.head x) (NE.tail x)

shift :: NonEmpty Pitch -> NonEmpty Pitch
shift (x :| []) = x :| []
shift (x :| xs) =
  let f = if snd x <= snd (last xs) then second (flip (-) 1) else id
  in  f (last xs) :| (x : init xs)

inversions xs = take (length xs) . iterate shift $ xs

orderChord xs =
  let r        = Elab.inversions xs
      classify = classifyIntervals . NE.fromList . chordToIntervals
      results  = fmap classify r
  in  fmap (MkOrdChord . NE.nub) . foldl' (<|>) Nothing $ zipWith (*>)
                                                                  results
                                                                  (Just <$> r)

class Elaborate r a where
  elaborate :: r -> a -> Music Pitch

randPick :: RandomGen g => g -> [a] -> Maybe (g, a)
randPick _ [] = Nothing
randPick g xs =
  let (choice, g') = (randomR (0, length xs - 1) g) in Just (g', xs !! choice)

allNotes = [A, As, Bf, B, C, Cs, Df, D, Ds, Ef, E, F, Fs, Gf, G, Gs, Af]

notePicker
  :: (Monad m, RandomGen r)
  => ([PitchClass] -> m [PitchClass])
  -> m (r -> Maybe (r, PitchClass))
notePicker f = do
  choices <- (f allNotes)
  pure $ flip randPick choices

keyFromPitch :: PitchClass -> [PitchClass]
keyFromPitch p = fst . flip trans (p, 0) <$> [0, 2, 4, 5, 7, 9, 11]

inKey :: (Monad m) => PitchClass -> [PitchClass] -> m [PitchClass]
inKey p = pure . intersect (keyFromPitch p)

test :: (RandomGen r) => Identity (r -> Maybe (r, PitchClass))
test = notePicker (inKey A)

data Numeral
  = N1 -- I
  | N2 -- IIb
  | N3 -- II
  | N4 -- IIIb
  | N5 -- III
  | N6 -- IV
  | N7 -- Vb
  | N8 -- V
  | N9 -- VIb
  | N10 -- VI
  | N11 -- VIIb
  | N12 -- VII
    deriving (Enum, Show, Eq, Ord)

data Chord
  = Maj Numeral
  | Min Numeral
  | Dim Numeral
    deriving (Show, Eq, Ord)

numeral :: Chord -> Numeral
numeral (Maj n) = n
numeral (Min n) = n
numeral (Dim n) = n

flatChord N2  = Just N2
flatChord N4  = Just N4
flatChord N7  = Just N7
flatChord N9  = Just N9
flatChord N11 = Just N11
flatChord _   = Nothing

chordNum N1  = "I"
chordNum N2  = "II"
chordNum N3  = "II"
chordNum N4  = "III"
chordNum N5  = "III"
chordNum N6  = "IV"
chordNum N7  = "V"
chordNum N8  = "V"
chordNum N9  = "VI"
chordNum N10 = "VI"
chordNum N11 = "VII"
chordNum N12 = "VII"

printChordNumeral :: Chord -> String
printChordNumeral (Maj c) = chordNum c <> maybe "" (const "b") (flatChord c)
printChordNumeral (Min c) =
  (toLower <$> chordNum c) <> maybe "" (const "b") (flatChord c)
printChordNumeral (Dim c) = printChordNumeral (Min c) <> "0"

shiftNum :: Int -> Numeral -> Numeral
shiftNum n = toEnum . (\x -> (x + n) `mod` 12) . fromEnum

-- Connects chords based on their proximity in the circle of fifths
-- Diminished chords are added, but do not connect with the other
-- related diminished chords
circleOfFifthsConn :: Chord -> [Chord]
circleOfFifthsConn (Maj n) =
  [Maj . shiftNum 7, Maj . shiftNum 5, Min . shiftNum 9, Dim . shiftNum 11]
    <*> [n]
circleOfFifthsConn (Min n) =
  [Min . shiftNum 7, Min . shiftNum 5, Maj . shiftNum 3, Dim . shiftNum 2]
    <*> [n]
circleOfFifthsConn (Dim n) = [Maj . shiftNum 2, Min . shiftNum 11] <*> [n]

-- Given some function that will yield the connections to
-- some element, label all other connected elements based
-- on their distance from the start
remoteness :: Eq a => (a -> [a]) -> a -> [(Natural, a)]
remoteness f c = go [(0, c)] []
 where
  go [] ys = ys
  go ((n, x) : xs) ys =
    let newConns =
          fmap ((,) (n + 1)) . filter (not . (`elem` (snd <$> ys))) $ f x
    in  go (xs <> newConns) (ys <> newConns)

tonalDecay :: Natural -> Probability
tonalDecay x = mkProbability $ ((-1) / 16) * (fromIntegral x + 8) ^ 2 + 8

list :: b -> (a -> [a] -> b) -> [a] -> b
list x _ []       = x
list _ f (x : xs) = f x xs

chordRemoteDist =
  mkDist
    . Map.fromList
    . fmap (swap . first tonalDecay)
    . remoteness circleOfFifthsConn

pickNextChord
  :: RandomGen g
  => (Chord, Distribution Chord)
  -> g
  -> (g, (Chord, Distribution Chord))
pickNextChord (c, dist) g =
  maybe (g, (c, dist)) (second (flip (,) nextDist)) $ pickFromDist nextDist g
  where nextDist = chordRemoteDist c

withStdGen :: (StdGen -> b) -> IO b
withStdGen f = f <$> newStdGen

distDecay = overProbabilities
  (\x -> mkProbability (unProbability x - ((unProbability x ^ 2) + 1)))

generateProgression c g =
  let (g', c') = pickNextChord c g
  in  c : generateProgression (second distDecay c') g'

chrdTest =
  let start = Maj N1
  in  withStdGen
        (fmap (printChordNumeral . fst) . Prelude.take 8 . generateProgression
          (start, chordRemoteDist start)
        )

chordIntervals :: Chord -> NonEmpty Int
chordIntervals (Maj _) = major id
chordIntervals (Min _) = minor id
chordIntervals (Dim _) = dim id

instantiateChord :: Pitch -> Chord -> [Pitch]
instantiateChord p n =
  let p' = trans (fromEnum (numeral n)) p
  in  ((flip trans p')) <$> (0 : NE.toList (chordIntervals n))

rootUp :: Chord -> [Chord]
rootUp n =
  zipWith (.) [Maj, Min, Dim, Maj, Min, Dim] (shiftNum <$> [5, 5, 6, 8, 9, 9])
    <*> [numeral n]

-- There are more downward movements 
rootDown :: Chord -> [Chord]
rootDown n =
  zipWith (.) [Maj, Maj, Min, Maj, Min, Maj] (shiftNum <$> [3, 4, 4, 7, 7, 9])
    <*> [numeral n]

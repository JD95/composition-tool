module Intervals where

import           Control.Applicative
import           Control.Monad
import           Data.List                      ( sort )
import           Numeric.Natural
import           Data.List.NonEmpty             ( NonEmpty(..)
                                                , (<|)
                                                )
import qualified Data.List.NonEmpty            as NE

type Interval = Natural

data InversionSet
  = InversionSet
  { identity :: NonEmpty Natural
  , inversions :: [NonEmpty Natural]
  } deriving (Show, Eq, Ord)

rotate :: NonEmpty Interval -> NonEmpty Interval
rotate xs =
  let pivot = xs <> pure 12
  in  NE.fromList $ (flip (-) $ NE.head pivot) <$> (NE.tail pivot)

intervalPerm :: NonEmpty Interval -> InversionSet
intervalPerm notes = InversionSet (NE.head perms) (NE.tail perms)
 where
  extend xs = xs <> extend ((+ 12) <$> xs)
  perms = extend <$> NE.fromList
    (NE.take (NE.length notes + 1) (NE.iterate rotate (NE.sort notes)))

-- | An extendable function
ext :: (a -> b) -> a -> ((a -> a) -> b)
ext f x g = f (g x)

major f = f (4 :| [7])
minor f = f (3 :| [7])
aug f = f (4 :| [8])
dim f = f (3 :| [6])

maj7 = (<|) (11 :: Natural)
m7 = (<|) (10 :: Natural)
sus2 = (<|) (2 :: Natural)
sus4 = (<|) (5 :: Natural)

unextended :: InversionSet -> InversionSet
unextended iset =
  let f = NE.takeWhile (< (12 :: Natural))
  in  InversionSet (NE.fromList $ f (identity iset))
                   (NE.fromList . f <$> (inversions iset))

inSet :: NonEmpty Natural -> NonEmpty Natural -> Maybe (NonEmpty Natural)
inSet xs set =
  guard (all (\x -> x `elem` NE.takeWhile (<= x) set) xs) *> Just set

data Exts = Maj7 | Min7 deriving (Show, Eq)
data Mods = Sus2 | Sus4 deriving (Show, Eq)
data Color = Major | Minor deriving (Show, Eq)
data ChordClass = ChordClass Color [Mods] [Exts] deriving (Show, Eq)

classifyIntervals :: NonEmpty Natural -> Maybe (ChordClass, NonEmpty Natural)
classifyIntervals xs =
  chrd Major [] [] (major id)
    <|> chrd Major [] [Min7] (major m7)
    <|> chrd Major [] [Maj7] (major maj7)
    <|> chrd Minor [] []     (minor id)
    <|> chrd Minor [] [Min7] (minor m7)
 where
  chrd color mods exts set = (,) (ChordClass color mods exts) <$> inSet xs (NE.sort set)

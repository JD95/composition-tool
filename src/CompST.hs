{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE TemplateHaskell #-}

module CompST where

import Lens.Micro
import Lens.Micro.TH
import Lens.Micro.GHC
import Euterpea ( PitchClass )
import Numeric.Natural
import Data.Monoid (Sum)
import Data.Semigroup
import System.Random

newtype Tension = Tension { unTension :: Int } deriving (Semigroup, Monoid, Eq, Ord) via (Sum Int)

data Triad = Triad { root :: PitchClass, third :: PitchClass, fifth :: PitchClass } deriving (Eq, Show)

data Goal
  = Goal
  { _goalDist :: Natural
  , _goalTension :: Tension
  , _goalChord :: Triad
  }

$(makeLenses ''Goal)

data CompST r
  = CompST
  { _currTen :: Tension
  , _currChord :: Triad
  , _currKey :: PitchClass
  , _tenGoals :: [Goal]
  , _rand :: r
  }

$(makeLenses ''CompST)

distToGoal x = x ^. tenGoals ^? _head . goalDist

currTenGoal x = x ^. tenGoals ^? each . goalTension

currGoal x = x ^. tenGoals ^? each

withCompRand :: RandomGen r => CompST r -> (r -> b) -> b
withCompRand c f = f (c ^. rand)

stepDist :: CompST r -> CompST r
stepDist c = c & tenGoals . ix 0 %~ goalDist -~ 1
